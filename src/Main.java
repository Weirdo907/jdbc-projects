import java.sql.*;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class Main {

	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/127.0.0.1";

	//  Database credentials
	static final String USER = "jdbc";
	static final String PASS = "2509";

	public static void main(String[] args) {
	Connection conn = null;
	Statement stmt = null;
	try{
	   //STEP 2: Register JDBC driver
	   Class.forName("com.mysql.jdbc.Driver");

	   //STEP 3: Open a connection
	   System.out.println("Connecting to database...");
	   conn = DriverManager.getConnection(DB_URL,USER,PASS);

	   //STEP 4: Execute a query
	   System.out.println("Creating statement...");
	   stmt = conn.createStatement();
	   String sql;
	   sql = "SELECT id, name, surname, age FROM Employees";
	   ResultSet rs = stmt.executeQuery(sql);

	   //STEP 5: Extract data from result set
	   while(rs.next()){
	      //Retrieve by column name
	      int id  = rs.getInt("id");
	      int age = rs.getInt("age");
	      String name = rs.getString("name");
	      String surname = rs.getString("surname");

	      //Display values
	      System.out.print("ID: " + id);
	      System.out.print(", Age: " + age);
	      System.out.print(", Name: " + name);
	      System.out.println(", Surname: " + surname);
	      
	   
	      Statement statement = conn.createStatement();
	      statement.executeUpdate("INSERT INTO Employees " + "VALUES (1001,'19''Lisa','Simpson',)");
	      stmt.execute("DELETE FROM EMPLOYEE WHERE ID >= 1");
	          
	            
	          }


	   //STEP 6: Clean-up environment
	   rs.close();
	   stmt.close();
	   conn.close();
	}catch(SQLException se){
	   //Handle errors for JDBC
	   se.printStackTrace();
	}catch(Exception e){
	   //Handle errors for Class.forName
	   e.printStackTrace();
	}finally{
	   //finally block used to close resources
	   try{
	      if(stmt!=null)
	         stmt.close();
	   }catch(SQLException se2){
	   }// nothing we can do
	   try{
	      if(conn!=null)
	         conn.close();
	   }catch(SQLException se){
	      se.printStackTrace();
	   }//end finally try
	}//end try
	System.out.println("Goodbye!");
	}//end main
	}//end FirstExampl
	   